package au.com.foxsports.jobqueue.example;

import java.util.HashMap;
import java.util.Map;

import org.mule.api.MuleContext;
import org.mule.api.MuleException;
import org.mule.api.context.MuleContextAware;
import org.mule.module.client.MuleClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.foxsports.commons.jobqueue.JobExecutor;
import au.com.foxsports.commons.jobqueue.QueueItemWrapper.Status;

public class CallbackJobExecutor implements JobExecutor<VideoDistributionWrapper>{
	private static final Logger logger = LoggerFactory.getLogger(CallbackJobExecutor.class);

	
	private MuleClient muleClient;
	@Override
	public VideoDistributionWrapper execute(VideoDistributionWrapper job) {
		logger.info("\n\nExecuting ********: {}", job);
		

		
		/*
		 * We can either execute the job here or dispatch the job back to Mule with 
		 * muleClient.dispatch 
		 * muleClient.send 
		 * 
		 * However, in that case we must do jobQueue.markAsDone(ID) so that the 
		 * executor framework can run the callback.
		 * 
		 * */

		
		job.setStatus(Status.DONE);
		
		return job;
	}
	public void setMuleClient(MuleClient muleClient) {
		this.muleClient = muleClient;
	}



}
