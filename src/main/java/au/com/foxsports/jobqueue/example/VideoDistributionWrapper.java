package au.com.foxsports.jobqueue.example;


import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

import au.com.foxsports.commons.jobqueue.QueueItemWrapper;
import au.com.foxsports.esb.distribution.commons.video.VideoDistributionJob;

@Document(indexName = "es_backed_queues", type = "incoming_job_queue")
public class VideoDistributionWrapper extends QueueItemWrapper {
	

    private VideoDistributionJob videoDistributionJob;
    private String coRelationId;
    
    public String getCoRelationId() {
		return coRelationId;
	}

	public void setCoRelationId(String coRelationId) {
		this.coRelationId = coRelationId;
	}

	public VideoDistributionWrapper() {
    }

    public VideoDistributionWrapper(VideoDistributionJob videoDistributionJob) {
        super(videoDistributionJob.getId());
        this.videoDistributionJob = videoDistributionJob;
    }

    @Override
    public String toString() {
        return this.coRelationId + ":" + videoDistributionJob;
    }

    public VideoDistributionJob getVideoDistributionJob() {
        return videoDistributionJob;
    }

    public void setVideoDistributionJob(VideoDistributionJob videoDistributionJob) {
        this.videoDistributionJob = videoDistributionJob;
    }


	public  Object getItem() {
		return videoDistributionJob;
	}
}
