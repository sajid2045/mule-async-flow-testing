package au.com.foxsports.jobqueue.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.mule.api.MuleMessage;
import org.mule.api.client.LocalMuleClient;
import org.mule.api.client.MuleClient;
import org.mule.tck.junit4.FunctionalTestCase;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import au.com.foxsports.commons.jobqueue.JobDispatcher;
import au.com.foxsports.commons.jobqueue.QueueItemWrapper;
import au.com.foxsports.commons.jobqueue.impl.elasticsearch.ESJobQueue;
import au.com.foxsports.esb.distribution.commons.video.VideoDistributionJob;

public class AsyncTest extends FunctionalTestCase {

	private ElasticsearchTemplate elasticsearchTemplate;

	@Override
	protected String getConfigResources() {
		return "spring-beans.xml,async-test.xml";
	}

	// @Test
	public void testSimpleVm() throws Exception {

		MuleClient muleClient = muleContext.getClient();

		VideoDistributionJob videoDistributionJob = new VideoDistributionJob();


		// videoDistributionJob.setId("XXXXX");

		muleClient.dispatch("vm://simple-vm-01", videoDistributionJob, null);

		MuleMessage request = muleClient.request("vm://out-vm-01", 100000);

		VideoDistributionJob payload = (VideoDistributionJob) request
				.getPayload();

		assertEquals("XXXX", payload.getId());
	}

	
	@Test
	public void testSpeedControl() throws Exception{
		
		JobDispatcher<VideoDistributionWrapper> jobDispatcher = muleContext.getRegistry()
				.get("jobDispatcher");
		
		MuleClient muleClient = muleContext.getClient();
		
		Map<String, Object> message = new HashMap<String, Object>();
		
		message.put("dispatchIntervalSec", 20);
		message.put("maxDispatchPerRun", 20);
		message.put("maxAllowedProcessing", 20);
		
		Thread.sleep(1000 * 2);
		muleClient.dispatch("vm://dispatch-speed-control", message, null);
		
		System.out.println("************************************************************************************************");
		
		Thread.sleep(10000);
		assertEquals(20,jobDispatcher.getDispatchIntervalSec());
		assertEquals(20,jobDispatcher.getMaxAllowedProcessing());
		assertEquals(20, jobDispatcher.getMaxDispatchPerRun());
		assertTrue(jobDispatcher.isRunning());
		
	}
	
	// @Test
	public void testAsyncRequestResponseFlow() throws Exception {
		MuleClient muleClient = muleContext.getClient();

		VideoDistributionJob videoDistributionJob = new VideoDistributionJob();

		// videoDistributionJob.setId("XXXXX");

		muleClient.dispatch("vm://async-request-response-01",
				videoDistributionJob, null);

		System.out.println("WAITING FOR JOB TO FINISH");
		MuleMessage request = muleClient.request("vm://async-result",
				1000 * 100);

		VideoDistributionJob payload = (VideoDistributionJob) request
				.getPayload();

		System.out.println("payload: " + payload);
		// this value was actually set by the groovy script in async flow
		assertEquals("PROVIDER", payload.getProviderName());
	}

	//@Test
	public void testAsyncFlowWithDispatcher() throws Exception {
		deleteOldData();

		MuleClient muleClient = muleContext.getClient();

		List<VideoDistributionJob> jobsDispatched = new ArrayList<VideoDistributionJob>();
		
		for (int i = 0; i < 10; i++) {
			VideoDistributionJob videoDistributionJob = new VideoDistributionJob();
			videoDistributionJob.setId(i +"_" + System.currentTimeMillis());
			Map<String, Object> props = new HashMap<String, Object>();

			props.put("MULE_CORRELATION_ID", videoDistributionJob.getId());

			videoDistributionJob.setMuleCoorelationId(videoDistributionJob.getId());
			videoDistributionJob.setMuleVmCallbackUrl("vm:///jobq-reply");
			
			jobsDispatched.add(videoDistributionJob);
			System.out.println("ORIG ID: " + props);
			muleClient.dispatch("vm://async-incoming-job",
					videoDistributionJob, props);
		}
		
		//verify you got all your response back in other words all the request-reply flow got unlocked and finished the flow.
		for(int i=0; i < 10; i++) {
			MuleMessage result = muleClient.request("vm://jobQ-result", 1000 * 15);
			assertNotNull(result);
			System.out.println("RESULT: " + result.getPayload(VideoDistributionJob.class));
			
		}

		//Thread.sleep(59 * 1000);
	}

	// @Test
	public void testAsyncJobQFlow() throws Exception {
		deleteOldData();

		MuleClient muleClient = muleContext.getClient();

		VideoDistributionJob videoDistributionJob = new VideoDistributionJob();
		String JOB_ID = "UID_" + System.currentTimeMillis();
		videoDistributionJob.setId(JOB_ID);

		Map<String, Object> props = new HashMap<String, Object>();

		props.put("MULE_CORRELATION_ID",
				"MY_UNIQUE_ID_" + System.currentTimeMillis());
		muleClient.dispatch("vm://async-incoming-job", videoDistributionJob,
				props);

		System.out.println("WAITING FOR JOB TO FINISH");

		Thread.sleep(1000 * 3);

		VideoDistributionJob videoDistributionJob2 = new VideoDistributionJob();
		videoDistributionJob2.setId(JOB_ID);
		videoDistributionJob2.setFileGroupName("FileGroup");

		/*
		 * This is to fool mule request-reply block so that it thinks it got the
		 * reply from the original thread. Question: Does it work with HA ? I
		 * can see local jvm level mutex used all over the place!
		 */
		muleClient.dispatch("vm:///jobq-reply", videoDistributionJob2, props);

		MuleMessage request = muleClient.request("vm://jobQ-result", 1000 * 15);

		assertNotNull(request);

		VideoDistributionJob payload = (VideoDistributionJob) request
				.getPayload();

		System.out.println("payload: " + payload);
		// this value was actually set by the groovy script in async flow
		// assertEquals("PROVIDER", payload.getProviderName());
		assertEquals(JOB_ID, payload.getId());

		Thread.sleep(1000 * 59);

	}

	private void deleteOldData() {
		ElasticsearchTemplate elasticsearchTemplate = muleContext.getRegistry()
				.get("elasticsearchTemplate");
		elasticsearchTemplate.deleteIndex(VideoDistributionWrapper.class);
	}

	public void code() {
		ESJobQueue<QueueItemWrapper> esJobQueue = null;
		esJobQueue
				.count(au.com.foxsports.commons.jobqueue.QueueItemWrapper.Status.QUEUED);
	}

}
